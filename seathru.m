clear all
clc
close all

dc = readraw;
% load /data/datasets/underwater-color-restoration/UnderwaterStereoDataset/Michmoret/image_set_01/xyzPoints.mat
%%

base_path = '/data/datasets/underwater-color-restoration/SeaThruDataset';

% suffix = '.ARW';
% depth_suffix = '.tif';
% mission = 'D1';
% left_idx = 'T_S03356';
% flip = true;

suffix = '.NEF';
depth_suffix = '.tif';
mission = 'D5';
left_idx = 'LFT_3411';
flip = true;

suffix = '.NEF';
depth_suffix = '.tif';
mission = 'D5';
left_idx = 'LFT_3392';
flip = true;

brighten = true;

depth_file = sprintf('%s/%s/depthMaps/depth%s%s', base_path, mission, left_idx, depth_suffix);
depth_info = imfinfo(depth_file);
dist_map_l = double(imread(depth_file));
left_file = sprintf('%s/%s/Raw/%s%s', base_path, mission, left_idx, suffix);
left_info = imfinfo(left_file);
left_img = double(imread(dc, left_file, '-w'));

if flip
    left_img = flipud(left_img);
end

left_img = imresize(left_img, size(dist_map_l));

if brighten
    left_img = imlocalbrighten(left_img,'AlphaBlend',true);
end

figure
subplot(1,2,1)
imshow(left_img, [])
subplot(1,2,2)
imshow(dist_map_l, [])

%%

left_gray = rgb2gray(left_img);
left_range = minmax(dist_map_l(:)');
intensity_range = minmax(left_img(:)');
unsqueezed_range = repmat(dist_map_l, [1 1 3]);
disp('Done loading')

%%

close all
ref_img = (left_img - intensity_range(1)) ./ (intensity_range(2) - intensity_range(1));

norm_dist = (unsqueezed_range - left_range(1)) ./ (left_range(2) - left_range(1));

midpoints = round(size(left_img) ./ 2);
ref_img(midpoints(1):end,1:midpoints(2),:) = norm_dist(midpoints(1):end,1:midpoints(2),:);
ref_img(1:midpoints(1),midpoints(2):end,:) = norm_dist(1:midpoints(1),midpoints(2):end,:);

figure;
imshow(ref_img)

%%
% right_range = minmax(dist_map_r(:)');

n_bins = 10;
left_bins = linspace(left_range(1), left_range(2), n_bins + 1);
% right_bins = linspace(right_range(1), right_range(2), n_bins + 1);

Omega = zeros(size(left_gray));
% total_searched = 0;
% total_selected = 0;
for i=1:n_bins
%     disp(i)
    search_zone = (left_bins(i) <= dist_map_l) & (dist_map_l <= left_bins(i+1));
%     total_searched = total_searched + length(search_zone);
    max_brightness = prctile(left_gray(search_zone), 1);
    mask = (left_gray < max_brightness) & search_zone;
    Omega = Omega | mask;
%     total_selected = total_selected + length(Omega{i});
%     disp(size( search_zone(search_zone <= max_brightness)) / size(search_zone))
%     left_masked(search_zone > max_brightness) = NaN;
end

% figure
% spy(Omega(:,:,1))

raw_ranges = repmat(dist_map_l(Omega), [1 3]);

left_img_r = left_img(:, :, 1);
left_img_g = left_img(:, :, 2);
left_img_b = left_img(:, :, 3);

raw_values = double([left_img_r(Omega) left_img_g(Omega) left_img_b(Omega)]);% / double(intmax(class(left_img)));

%% 
clc
model = @(x) reshape(x(1:3), [1 1 3]) .* (1 - exp(-reshape(x(4:6), [1 1 3])  .* unsqueezed_range)); %+ reshape(x(7:9), [1 1 3])  .* exp(-reshape(x(10:12), [1 1 3])  .* unsqueezed_range);
options = optimoptions('lsqnonlin');
options.MaxFunctionEvaluations = 2e3;
f = @(x) reshape(x(1:3), [1 3]) .* (1 - exp(-reshape(x(4:6), [1 3])  .* raw_ranges)) + reshape(x(7:9), [1 3])  .* exp(-reshape(x(10:12), [1 3])  .* raw_ranges) - raw_values;
eval = @(x) sqrt(sum(f(x) .^ 2, 'all'));
lb = zeros(1, 12);
ub = [1 1 1 5 5 5 1 1 1 5 5 5];
x = lsqnonlin(f, (lb + ub) ./ 2, lb, ub, options);

backscatter_est = model(x);
D = left_img - min(backscatter_est, left_img);

%%
figure
subplot(2,2,1)
imshow(left_img)
subplot(2,2,2)
imshow(Omega)
subplot(2,2,3)
imshow(backscatter_est)
subplot(2,2,4)
imshow(D)